## Git Lab Radiator

This projects contain a simple method that perform an sum together with a couple of test,
just to allow us to quickly make a gitlab CI build failed or pass with small changes.

### Running Tests
```
./gradlew test
```
