package com.gitlab.ci.radiator;

public class Sum {

    public int evaluate(int x, int y) {
        return x + y;
    }
}
