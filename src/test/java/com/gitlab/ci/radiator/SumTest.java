package com.gitlab.ci.radiator;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class SumTest {
    private Sum sum = new Sum();

    @Test
    public void shouldBeZeroIfSumZeroAndZero() {
        assertThat(sum.evaluate(0,0), is(0));
    }

    @Test
    public void shouldBeTenIfSumZeroAndTen() {
        assertThat(sum.evaluate(0,10), is(10));
    }

    @Test
    public void shouldBeElevenIfSumOneAndTen() {
        assertThat(sum.evaluate(1,10), is(11));
    }
}